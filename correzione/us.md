User stories totali (fatte) 11/11 
Commento US: Generalmente accettabili. Manca qualsiasi riferimento al fatto che l'input derivi dalla chiamate rest al server; le storie sono state aggiornate per eliminare quelle errate, ma non ne sono state aggiunte o chiarite - per quanto si può dedurre dal log di trello.Stimate: no, Prioritizzate: pare di no.
Organizzazione in sprint (velocity): no -   media 2.2 US al giorno, ma non si può capire dai dati forniti.

CRITICA
as a user i would like that:
1-calculate the maximum theoretical gain achieved in the past days
	UNCLEAR WHAT PAST DAYS MEANS
2-calculate today's earning possibilities
	THIS IS NOT PART OF THE REQUIREMENTS
3-print the maximum theoretical gain achieved in the past days
	OK
4-print today's earning possibilities
	SEE ABOVE
5-print the bag logo
	BAG? 
6-print the name of the bag
	BAG?
7-print the description of the bag
	BAG?
8-print the stock exchange title code
	OK
9-print the calculated data of the bag
	INCOMPREHENSIBLE - OR TOO BIG
10-print last month's stock market chart
	NOT PART OF THE REQUIREMENTS
11-print the stock market chart in a chosen amount of time
	OK

