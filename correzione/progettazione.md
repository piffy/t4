Valutazione Torricelli: 3.86 (media)
Evoluzione progettazione:La prima versione dell'UML era abbastanza orribile. La seconda release non ha UML.

Critica UML
La progettazione prevede 6 oggetti, che rispecchia il codice finale, ma il sospetto è che sia un retroject, cosa corroborata dalla presenza di una classe che non si usa affatto.  In ogni caso, mancano alcuni metodi (per esempio in Print)i metodi static non sono indicati come tali. In generale, manca uno spirito OOP, e l'approccio è assai procedurale. 
Per esempio, l'dea di aver action che estende apifetch il non aver capito il senso della generalizzazione: si potrebbe (e, specificamente, questo programma potrebbe) usare apifetch senza action? Ovviamente no, infatti logicamente è un'entità sola. Un blob di codice.




Critica Codice
Il punto "vincente" del programma è stato l'uso della libreria YahooFinance, che ha semplificato parecchio le cose. Tuttavia dover utilizzare dati della libreria ha condizionato la scrittura dell'intero codice, rendendo difficile anche la lettura. La soluzione usata è stata quella di salvare tutto su disco.

Static code Analysis: C
apifetch.java: è certamente il "cuore" dell'applicazione. Il codice è un po' complesso, ma ad occhio recupera i dati dall'url del cliente e prepara i dati. Non capisco l'uso di BigDecimal, non mi pare un'app che richieda una tale raffinatezza. Tutti i dati e le funzioni sono statici, probabilmente si potevano organizzare meglio (ma il programma andava reimpostato)

action.java: "estende" apifetch. 

print.java: programmino di stampa. Non utilizzato.

save.java: Programma che riceve i dati e li porta in formato json.










 




	
	
