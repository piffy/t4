# T4 Sprint 1 checklist

* Documentazione di progettazione aggiornata: PARTIAL (UML ancora buggato); 
* Sprint backlog aggiornato su trello: PARTIAL (ci sono le issues ma non vedo US, "sprint" non si capisce che è"
* Codice : NO (Presente in altri branch, ma non si capisce qual è quello giusto.)
* Codice JUnit : NO
* JavaDoc: NO
* Documentazione in formato gittabile (grafici esclusi) : NO (bonus)
* Diario giornaliero delle attività personali (incluse ore fuori orario) : NO (bonus)
* Daily status: NO 
* Review video: PARTIAL (muto, US non mostrata, codice non si sa quale è eseguito)
* Retrospettiva : OK 


