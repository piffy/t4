# T4 Sprint 2 checklist

* Documentazione di progettazione aggiornata: PARTIAL (UML sparito?); 
* Sprint backlog aggiornato su trello: OK
* Codice : OK 
* Codice JUnit : NO
* JavaDoc: NO
* Company logo: NO
* Documentazione in formato gittabile (grafici esclusi) : PARTIAL (bonus)
* Diario giornaliero delle attività personali (incluse ore fuori orario) : NO (bonus)
* Daily status: OK 
* Review video: NOT IN ENGLISH - DOES NOT SHOW USER STORIES - VERY LATE
* Retrospective : RITARDO - un po' di confusione tra sintomi e cura



