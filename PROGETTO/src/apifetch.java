import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import yahoofinance.Stock;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.lang.System.out;
/**
 * This class takes dates and ranges and uses them to represent graphs
 */
public class apifetch{

    protected static Stock stock;
    protected static List<HistoricalQuote> rawData1;
    protected static List<HistoricalQuote> rawData2;
    static ArrayList<BigDecimal> stockList1stGraph = new ArrayList<java.math.BigDecimal>();
    static ArrayList<Long> volumeValue = new ArrayList<Long>();
    static ArrayList<String> stockList1stGraphDate = new ArrayList<String>();
    static ArrayList<String> stockList2ndGraphDate = new ArrayList<String>();
    static ArrayList<BigDecimal> openValue = new ArrayList<java.math.BigDecimal>();
    static ArrayList<BigDecimal> closeValue = new ArrayList<java.math.BigDecimal>();
    static ArrayList<BigDecimal> highValue = new ArrayList<java.math.BigDecimal>();
    static ArrayList<BigDecimal> lowValue = new ArrayList<java.math.BigDecimal>();

    static Calendar from2 = Calendar.getInstance();
    static Calendar to2 = Calendar.getInstance();
    static Calendar from = Calendar.getInstance();
    static Calendar to = Calendar.getInstance();
    /**
     *
     * @return
     * @throws IOException
     * @throws InterruptedException
     * takes data from the customer's site
     */
    static String getMainData() throws IOException, InterruptedException {

        var client = HttpClient.newBuilder().build();
        var request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://dwweb.gnet.it/dw2022/"))
                .build();

        var response = client.send(request, HttpResponse.BodyHandlers.ofString());
        out.printf("Response code is: %d %n",response.statusCode()); //STAMPA RISPOSTA RICHIESTA
        out.printf("The response body is:%n %s %n", response.body()); //STAMPA RISULTATO RICHIESTA

        return response.body();

    }
    /**
     *
     * @return
     * use this function to get the stock exchange code
     */
    static String getTicker() {

        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("json/settingsvalue.json"));
            JSONObject jsonObject = (JSONObject) obj;

            return (String) jsonObject.get("ticker");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return "Error! JSONObject not found!";
    }
    /**
     *
     * @return
     * gets the start date of the tracking
     */
    static String getStartDate() {

        String inizio;

        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("json/settingsvalue.json"));
            JSONObject jsonObject = (JSONObject) obj;

            inizio = (String) jsonObject.get("starting_date");

            DateTimeFormatter fIn = DateTimeFormatter.ofPattern("dd/MM/uuuu", Locale.UK);  // As a habit, specify the desired/expected locale, though in this case the locale is irrelevant.
            LocalDate ld = LocalDate.parse(inizio, fIn);

            DateTimeFormatter fOut = DateTimeFormatter.ofPattern("uuuu/MM/dd", Locale.UK);

            return ld.format(fOut);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return null;

    }
    /**
     *
     * @return
     * get the tracking end date
     */
    static String getEndDate() {

        String fine;

        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("json/settingsvalue.json"));
            JSONObject jsonObject = (JSONObject) obj;

            fine = (String) jsonObject.get("ending_date");

            DateTimeFormatter fIn = DateTimeFormatter.ofPattern("dd/MM/uuuu", Locale.UK);  // As a habit, specify the desired/expected locale, though in this case the locale is irrelevant.
            LocalDate ld = LocalDate.parse(fine, fIn);

            DateTimeFormatter fOut = DateTimeFormatter.ofPattern("uuuu/MM/dd", Locale.UK);

            return ld.format(fOut);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return null;

    }
    /**
     *
     * @throws IOException
     * @throws InterruptedException
     * gets the data to build the first graph
     */
    static void get1stGraphValue() throws IOException, InterruptedException {

        //stock.getQuote().getAvgVolume().toString();

        from.add(Calendar.MONTH, -23); // from x month ago
        to.add(Calendar.MONTH, 1);
        rawData1 = stock.getHistory(from, to, Interval.MONTHLY);

        for (int i = 0; i < rawData1.size(); i++){

            stockList1stGraph.add(rawData1.get(i).getClose());
            stockList1stGraphDate.add("\"" + rawData1.get(i).getDate().get(Calendar.YEAR) + "-" + (rawData1.get(i).getDate().get(Calendar.MONTH) + 1) + "-" + rawData1.get(i).getDate().get(Calendar.DATE) + "\"");

        }

    }
    /**
     *
     * @throws IOException
     * @throws InterruptedException
     * @throws java.text.ParseException
     * gets the data to build the second graph
     */
    static void get2ndGraphValue() throws IOException, InterruptedException, java.text.ParseException {

        Date start = new SimpleDateFormat("yyyy/MM/dd").parse(getStartDate());
        from2.setTime(start);
        from2.add(Calendar.DATE, 1);

        Date end = new SimpleDateFormat("yyyy/MM/dd").parse(getEndDate());
        to2.setTime(end);
        to2.add(Calendar.DATE, 1);

        rawData2 = stock.getHistory(from2, to2, Interval.DAILY);

        for (int i = 0; i < rawData2.size(); i++){

            openValue.add(rawData2.get(i).getOpen());
            closeValue.add(rawData2.get(i).getClose());
            highValue.add(rawData2.get(i).getHigh());
            lowValue.add(rawData2.get(i).getLow());

            volumeValue.add(rawData2.get(i).getVolume());
            stockList2ndGraphDate.add("\"" + rawData2.get(i).getDate().get(Calendar.YEAR) + "-" + (rawData2.get(i).getDate().get(Calendar.MONTH) + 1) + "-" + rawData2.get(i).getDate().get(Calendar.DATE) + "\"");

        }

    }

}
