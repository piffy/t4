import org.json.simple.JSONArray;

import java.awt.*;
import java.io.File;
import java.io.IOException;
/**
 *
 *the actions class aims to open the site and create the json file for the site
 */
public class actions extends apifetch{
    /**
     *
     * @throws IOException
     * opens the site, loading the page URL
     */
    static void openWebsite() throws IOException {

        String url2 = "WEB INTERFACE/index.html";
        File htmlFile = new File(url2);
        Desktop.getDesktop().browse(htmlFile.toURI());

    }
    /**
     *
     * @param value1stGraph
     * @param value2ndGraph
     * create the JSON file
     */
    public static void buildJSONFile(JSONArray value1stGraph, JSONArray value2ndGraph) {

        //value1stGraph.addAll(stockList1stGraph);

        value2ndGraph.addAll(volumeValue);

    }
    /**
     *
     * @param value1stGraph
     * @param value2ndGraph
     * reset the data otherwise it will overwrite
     */

    public static void resetAll(JSONArray value1stGraph, JSONArray value2ndGraph){

        stockList1stGraph.clear();
        volumeValue.clear();

        stockList1stGraphDate.clear();
        stockList2ndGraphDate.clear();

        value1stGraph.clear();
        value2ndGraph.clear();

        //rawData1.clear();
        rawData2.clear();

        openValue.clear();
        closeValue.clear();
        highValue.clear();
        lowValue.clear();

    }

}
