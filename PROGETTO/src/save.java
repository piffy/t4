import org.json.simple.JSONArray;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/**
 *The save class saves the data taken from the API in the attached json files in the "PROGETTO/WEB INTERFACE" folder
 */
public class save {
    /**
     *
     * @param mainApi
     *save the JSON containing the result of the call
     *to the customer's site
     */
    static void saveJSONConfigFile(String mainApi){

        try {
            File myObj = new File("json/settingsvalue.json");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter("json/settingsvalue.json");
            myWriter.write(mainApi);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
    /**
     *
     * @param value1stGraph
     * @param value2ndGraph
     * save the JSON containing the data to build the graphs
     */
    static void saveJSONFile(JSONArray value1stGraph, JSONArray value2ndGraph){

        String path1stGraph = "WEB INTERFACE/json/data_1stGraph.json";
        String path2ndGraph = "WEB INTERFACE/json/data_2ndGraph.json";
        String path1stGraphDate = "WEB INTERFACE/json/data_1stGraphDate.json";
        String path2ndGraphDate = "WEB INTERFACE/json/data_2ndGraphDate.json";
        String pathCloseGraph = "WEB INTERFACE/json/data_closeGraph.json";
        String pathOpenGraph = "WEB INTERFACE/json/data_openGraph.json";
        String pathDayHighGraph = "WEB INTERFACE/json/data_dayHighGraph.json";
        String pathDayLowGraph = "WEB INTERFACE/json/data_dayLowGraph.json";
        String pathDataCalculation = "WEB INTERFACE/json/data_calculated.json";

        // ------------------------------------------------------------------

        try {
            File myObj = new File(path1stGraph);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                myObj.delete();
                myObj.createNewFile();
                System.out.println("File already exists. Re-created!");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(path1stGraph);
            myWriter.write("dataFirstGraph = '{\"fullname\":\"" + apifetch.stock.getName() + "\",\"stocksymbol\":\"" + apifetch.stock.getSymbol() + "\",\"mercato\":\"" + apifetch.stock.getStockExchange() + "\"," + calculations.getMoreFrequentValue() + "}'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(path2ndGraph);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(path2ndGraph);
            myWriter.write("dataSecondGraph = '" + value2ndGraph.toJSONString() + "'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(pathCloseGraph);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(pathCloseGraph);
            myWriter.write("dataCloseGraph = '" + apifetch.closeValue.toString() + "'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(pathOpenGraph);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(pathOpenGraph);
            myWriter.write("dataOpenGraph = '" + apifetch.openValue.toString() + "'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(pathDayHighGraph);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(pathDayHighGraph);
            myWriter.write("dataDayHighGraph = '" + apifetch.highValue.toString() + "'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(pathDayLowGraph);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(pathDayLowGraph);
            myWriter.write("dataDayLowGraph = '" + apifetch.lowValue.toString() + "'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(pathDataCalculation);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(pathDataCalculation);
            myWriter.write("dataCalculated = '{\"maxTheoretical\":\"" + calculations.maxTheoretical() + "\",\"earnInPeriod\":\"" + calculations.earnInPeriod() + "\",\"growthPerc\":\"" + calculations.growthPerc() + "\"}'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(path1stGraphDate);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(path1stGraphDate);
            myWriter.write("firstGraphDate = '{\"results\":" + apifetch.stockList1stGraphDate.toString() + "}'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

        try {
            File myObj = new File(path2ndGraphDate);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(path2ndGraphDate);
            myWriter.write("secondGraphDate = '{\"results\":" + apifetch.stockList2ndGraphDate.toString() + "}'");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        // ------------------------------------------------------------------

    }

}
