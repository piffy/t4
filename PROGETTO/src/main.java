import org.apache.log4j.BasicConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import yahoofinance.YahooFinance;

import java.io.IOException;

import static java.lang.Thread.sleep;
/**
 * The main allows to obtain the user's data from the site https://dwweb.gnet.it/dw2022/,
 * then going to initialize the connection with the API.
 * Then create the 2 graphs with the different descriptions of the stock
 */
public class main {

    static String nome_borsa;
    /**
     *
     * @param args
     * @throws IOException
     * @throws InterruptedException
     * @throws ParseException
     * @throws java.text.ParseException
     * repeats the actions in a loop
     */
    public static void main(String[] args) throws IOException, InterruptedException, ParseException, java.text.ParseException {

        JSONArray value1stGraph = new JSONArray();
        JSONArray value2ndGraph = new JSONArray();

        int temp = 0;

        while (true){

            String mainApi = apifetch.getMainData();

            save.saveJSONConfigFile(mainApi);

            nome_borsa = apifetch.getTicker();
            //nome_borsa = "AAPL";

            BasicConfigurator.configure(); //fondamentale

            apifetch.stock = YahooFinance.get(nome_borsa); //inizializza la connessione con l'API

            //File myObj = new File("WEB INTERFACE/json/temp2.json");
            //myObj.createNewFile();

            //apifetch.get1stGraphValue(); //API RESPONSE for FIRST GRAPH

            apifetch.get2ndGraphValue(); //API RESPONSE for SECOND GRAPH

            actions.buildJSONFile(value1stGraph, value2ndGraph);
            save.saveJSONFile(value1stGraph, value2ndGraph);

            actions.resetAll(value1stGraph, value2ndGraph);

            if (temp == 0){

                actions.openWebsite(); //apre la pagina web
                temp++;

            }

            sleep(10000);

        }

    }

}
