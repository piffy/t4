import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
/**
 *processes the necessary operations requested by the user
 */
public class calculations {

    static ArrayList<Integer> valueCastedToInt = new ArrayList<Integer>();
    /**
     *
     * @return
     * calculates the theoretical maximum achievable
     */
    static double maxTheoretical(){

        double result = 0;

        for (int i = 0; i < apifetch.rawData2.size(); i++){
            result = result + (apifetch.rawData2.get(i).getHigh().doubleValue() - apifetch.rawData2.get(i).getLow().doubleValue());
        }

        return result;

    }
    /**
     *
     * @return
     * @throws IOException
     * obtains the profit of the analyzed period
     */
    static double earnInPeriod() throws IOException {

            BigDecimal lastdayClose = apifetch.rawData2.get(apifetch.rawData2.size() - 1).getClose();
            BigDecimal firstdayOpen = apifetch.rawData2.get(0).getOpen();

            return lastdayClose.doubleValue() - firstdayOpen.doubleValue();

    }
    /**
     *
     * @return
     * obtains the percentage of growth of the analyzed period
     */
    static double growthPerc(){

        int growDay = 0;

        for (int i = 0; i < apifetch.rawData2.size(); i++){
            if (apifetch.rawData2.get(i).getClose().doubleValue() - apifetch.rawData2.get(i).getOpen().doubleValue() > 0) growDay++;
        }

        return (growDay * 100.00) / apifetch.rawData2.size();

    }
    /**
     *
     * @return
     * it is used to obtain the most frequent values of the analyzed period
     */
    static String getMoreFrequentValue() {

        ArrayList<Integer> tempValues1 = new ArrayList<Integer>();
        ArrayList<Integer> tempValues2 = new ArrayList<Integer>();
        ArrayList<Integer> tempValues3 = new ArrayList<Integer>();

        for (int i = 0; i < apifetch.rawData2.size(); i++) {

            valueCastedToInt.add((int) apifetch.rawData2.get(i).getClose().doubleValue()); //aggiunge un valore

        }

        Integer top1
                = valueCastedToInt.stream()
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .get()
                .getKey();

        int top1count = 0;

        for (int i = 0; i < valueCastedToInt.size(); i++){

            if (Objects.equals(valueCastedToInt.get(i), top1)){

                valueCastedToInt.remove(i);
                tempValues1.add(top1);
                top1count++;

            }

        }

        //----------------------------------------

        Integer top2
                = valueCastedToInt.stream()
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .get()
                .getKey();

        int top2count = 0;

        for (int i = 0; i < valueCastedToInt.size(); i++){

            if (Objects.equals(valueCastedToInt.get(i), top2)){

                valueCastedToInt.remove(i);
                tempValues2.add(top2);
                top2count++;

            }

        }

        //----------------------------------------

        Integer top3
                = valueCastedToInt.stream()
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .get()
                .getKey();

        int top3count = 0;

        for (int i = 0; i < valueCastedToInt.size(); i++){

            if (Objects.equals(valueCastedToInt.get(i), top3)){

                valueCastedToInt.remove(i);
                tempValues3.add(top3);
                top3count++;

            }

        }

        valueCastedToInt.addAll(tempValues1);
        valueCastedToInt.addAll(tempValues2);
        valueCastedToInt.addAll(tempValues3);

        return "\"values\":[\"" + top1 + "\",\"" + top2 + "\",\"" + top3 + "\"],\"repeated\":[\"" + top1count + "\",\"" + top2count + "\",\"" + top3count + "\"]";

    }


}
